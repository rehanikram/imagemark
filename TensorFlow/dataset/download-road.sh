
# the result is
# echo "downloading road dataset , downloading"
# wget https://s3-ap-northeast-1.amazonaws.com/mycityreport/RoadDamageDataset.tar.gz

# tar -xvzf RoadDamageDataset.tar.gz

export PYTHONPATH=../source/
python create_road_tf_record.py \
    --label_map_path=road_label_map.pbtxt \
    --data_dir=RoadDamageDataset  --set=train \
    --output_path=road_train.record
python create_road_tf_record.py \
    --label_map_path=road_label_map.pbtxt \
    --data_dir=RoadDamageDataset  --set=val \
    --output_path=road_val.record

