

r"""Convert raw Road dataset to TFRecord for object_detection.

Example usage:
    python object_detection/dataset_tools/create_road_tf_record.py \
        --data_dir=/home/user/VOCdevkit \
        --year=VOC2012 \
        --output_path=/home/user/pascal.record
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import hashlib
import io
import logging
import os
import fnmatch
import tensorflow as tf

import contextlib2
# from google3.third_party.tensorflow_models.object_detection.dataset_tools import tf_record_creation_util


from lxml import etree
import PIL.Image
import tensorflow as tf

from object_detection.utils import dataset_util
from object_detection.utils import label_map_util

flags = tf.app.flags
flags.DEFINE_string('data_dir', '', 'Root directory to raw PASCAL VOC dataset.')
flags.DEFINE_string('set', 'train', 'Convert training set, validation set or '
                    'merged set.')
flags.DEFINE_string('annotations_dir', 'Annotations',
                    '(Relative) path to annotations directory.')
# flags.DEFINE_string('year', 'VOC2007', 'Desired challenge year.')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('label_map_path', 'data/road_label_map.pbtxt',
                    'Path to label map proto')
flags.DEFINE_boolean('ignore_difficult_instances', False, 'Whether to ignore '
                     'difficult instances')
FLAGS = flags.FLAGS

SETS = ['train', 'val', 'trainval', 'test']
# YEARS = ['VOC2007', 'VOC2012', 'merged']


def create_tf_example(data,
                       dataset_directory,
                       label_map_dict,
                      image_subdirectory='JPEGImages'):
  """Convert XML derived dict to tf.Example proto."""

  # TODO(user): Populate the following variables from your example.
  height = None # Image height
  width = None # Image width
  filename = None # Filename of the image. Empty if image is not from file
  encoded_image_data = None # Encoded image bytes
  image_format = None # b'jpeg' or b'png'

  img_path = os.path.join(data['folder'], image_subdirectory, data['filename'])
  full_path = os.path.join(dataset_directory, img_path)
  with tf.gfile.GFile(full_path, 'rb') as fid:
      encoded_jpg = fid.read()
  encoded_jpg_io = io.BytesIO(encoded_jpg)
  image = PIL.Image.open(encoded_jpg_io)
  if image.format != 'JPEG':
      raise ValueError('Image format not JPEG')
  key = hashlib.sha256(encoded_jpg).hexdigest()

  width = int(data['size']['width'])
  height = int(data['size']['height'])



  xmin = [] # List of normalized left x coordinates in bounding box (1 per box)
  xmax = [] # List of normalized right x coordinates in bounding box
             # (1 per box)
  ymin = [] # List of normalized top y coordinates in bounding box (1 per box)
  ymax = [] # List of normalized bottom y coordinates in bounding box
             # (1 per box)
  classes_text = [] # List of string class name of bounding box (1 per box)
  classes = [] # List of integer class id of bounding box (1 per box)

  #truncated = []
  #poses = []
  #difficult_obj = []
  if 'object' in data:
      pass
  else:
      return

  for obj in data['object']:

      xmin.append(float(obj['bndbox']['xmin']) / width)
      ymin.append(float(obj['bndbox']['ymin']) / height)
      xmax.append(float(obj['bndbox']['xmax']) / width)
      ymax.append(float(obj['bndbox']['ymax']) / height)
      classes_text.append(obj['name'].encode('utf8'))
      classes.append(label_map_dict[obj['name']])


  tf_example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(height),
      'image/width': dataset_util.int64_feature(width),
      'image/filename': dataset_util.bytes_feature(data['filename'].encode('utf8')),
      'image/source_id': dataset_util.bytes_feature(data['filename'].encode('utf8')),
      'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
      'image/encoded': dataset_util.bytes_feature(encoded_jpg),
      'image/format': dataset_util.bytes_feature('jpeg'.encode('utf8')),
      'image/object/bbox/xmin': dataset_util.float_list_feature(xmin),
      'image/object/bbox/xmax': dataset_util.float_list_feature(xmax),
      'image/object/bbox/ymin': dataset_util.float_list_feature(ymin),
      'image/object/bbox/ymax': dataset_util.float_list_feature(ymax),
      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
  }))
  return tf_example


def main(_):
  if FLAGS.set not in SETS:
    raise ValueError('set must be in : {}'.format(SETS))
  data_dir = FLAGS.data_dir

  writer = tf.python_io.TFRecordWriter(FLAGS.output_path)
  label_map_dict = label_map_util.get_label_map_dict(FLAGS.label_map_path)
  # TODO(user): Write code to read in your dataset to examples variable
  people=['Adachi','Chiba','Ichihara','Muroran','Nagakute','Numazu','Sumida']
  totalCount=0

  for person in people:
    logging.info('Reading from Road %s dataset.', person)
    examples_path = os.path.join(data_dir, person, 'ImageSets', 'Main',  FLAGS.set + '.txt')
    annotations_dir = os.path.join(data_dir, person, FLAGS.annotations_dir)
    examples_list = dataset_util.read_examples_list(examples_path)

    for idx, example in enumerate(examples_list):
      if idx % 100 == 0:
        logging.info('On image %d of %d', idx, len(examples_list))
      path = os.path.join(annotations_dir, example + '.xml')
      with tf.gfile.GFile(path, 'r') as fid:
          xml_str = fid.read().encode('utf-8')
      #parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
      xml = etree.fromstring(xml_str)
      data = dataset_util.recursive_parse_xml_to_dict(xml)['annotation']

      tf_example = create_tf_example(data, FLAGS.data_dir, label_map_dict)
      if tf_example is not None:
        writer.write(tf_example.SerializeToString())
        totalCount+=1
      else:
        print("tf example is none:"+path)
  writer.close()
  print("Total number of files processed:" + str(totalCount))


  # for person in people:
  #   examplesFiles = fnmatch.filter(os.listdir(os.path.join(data_dir,person,'JPEGImages')), FLAGS.set+'*.jpg')
  #   examples= [os.path.splitext(x)[0] for x in examplesFiles]
  #
  #
  #   for example in examples:
  #     annotations_dir = os.path.join(data_dir,person ,FLAGS.annotations_dir)
  #     path = os.path.join(annotations_dir, example + '.xml')
  #     with tf.gfile.GFile(path, 'r') as fid:
  #         xml_str = fid.read().encode('utf-8')
  #     parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
  #     xml = etree.fromstring(xml_str,parser)
  #     data = dataset_util.recursive_parse_xml_to_dict(xml)['annotation']
  #
  #     tf_example = create_tf_example(data, FLAGS.data_dir, label_map_dict)
  #     if tf_example is not None:
  #       writer.write(tf_example.SerializeToString())
  #
  # writer.close()


if __name__ == '__main__':
  tf.app.run()